package fr.afpa.cda.bared.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Personne;

public interface IPersonneService {
	public List<Personne> findAll();

	public Personne findById(Integer id);

	public Personne save(Personne nouvellePersonne,MultipartFile file) throws IOException;

	public Personne update(Personne nouvellePersonne,MultipartFile file) throws IOException;

	public void deleteById(Integer id);

	public Personne findByEmailAndPassword(String login, String password);

//	public Personne update(Personne nouveauPersonne);
//
//	public Personne save(Personne nouveauPersonne);
}
