package fr.afpa.cda.bared.webapp.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Adresse;
import fr.afpa.cda.bared.webapp.bean.Joueur;
import fr.afpa.cda.bared.webapp.bean.Personne;
import fr.afpa.cda.bared.webapp.services.IJoueurService;

@Controller
public class JoueurController {
	@Autowired
	private IJoueurService joueurService;
	
	@GetMapping("/profilJoueur")
	public String afficherProfil(HttpSession session) {
//		if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
		if (session.isNew() || session.getAttribute("connectedUser")==null)
			return "redirect:/profil/login";
		return "profil/profil_joueur_page";
	}

	@GetMapping("/profilJoueur/modifier")
	public String afficherModifierProfil(HttpSession session) {
//		if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
		if (session.isNew() || session.getAttribute("connectedUser")==null)
			return "redirect:/profil/login";
		return "profil/modifier_joueur_page";
	}

	@PostMapping("/profilJoueur/modifier")
	public String modifierProfilJOueur(HttpSession session, @RequestParam("file_image") MultipartFile file,
			Joueur joueurModifie, Adresse adresse) throws IOException {
		joueurModifie.setAdresse(adresse);
		joueurModifie = joueurService.update(joueurModifie, null);
		session.setAttribute("connectedUser", joueurModifie);
		return "redirect:/";
	}

	@GetMapping("/profilJoueur/inscription")
	public String afficherInscriptionProfil() {
		System.out.println("Passe");
		return "profil/nouveau_joueur_page";
	}
	
//	@PostMapping("/profilJoueur/inscription")
//	public String inscriptionProfil(HttpSession session, Joueur nouveauJoueur, Adresse adresse) throws IOException {
//		nouveauJoueur.setAdresse(adresse);
//		System.out.println(nouveauJoueur);
//		nouveauJoueur = joueurService.save(nouveauJoueur,null);
//		session.setAttribute("connectedUser", nouveauJoueur);
//		return "redirect:/";
//	}

	@PostMapping("/profilJoueur/inscription")
	public String inscriptionProfil(HttpSession session, @RequestParam("file_image") MultipartFile file,
			Joueur nouveauJoueur, Adresse adresse) throws IOException {
		nouveauJoueur.setAdresse(((Personne)session.getAttribute("connectedUser")).getAdresse());
		nouveauJoueur = joueurService.save(nouveauJoueur, file);
		session.setAttribute("connectedUser", nouveauJoueur);
		session.setAttribute("joueur", true);

		return "redirect:/profilJoueur";
	}

//	@GetMapping({"/profil/login","/login"})
//	public String afficherSeLoger(/*String login, String password*/) throws Exception {
////		if(true) {
////			throw new Exception("toto");
////		}
//		return "profil/login_page";
//	}
//
//	@GetMapping("/profil/logout")
//	public String logout(HttpSession session) {
//		session.invalidate();
//		return "redirect:/profil/login";
//	}

	@ExceptionHandler(Exception.class)
	public String handleNotFoudPage(Exception exc) {
		exc.printStackTrace();
		return "errors/not_found";
	}
}
