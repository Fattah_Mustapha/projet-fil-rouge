package fr.afpa.cda.bared.webapp.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Personne {
	private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private String clubPrefere;
	private String clubActuel;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateNaissance;
	private String telephone;
	private Adresse adresse;
}
