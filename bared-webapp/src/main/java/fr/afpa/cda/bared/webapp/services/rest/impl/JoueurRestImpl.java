package fr.afpa.cda.bared.webapp.services.rest.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Joueur;
import fr.afpa.cda.bared.webapp.properties.MicroserviceConfigProperties;
import fr.afpa.cda.bared.webapp.services.rest.IJoueurRest;

@Service
public class JoueurRestImpl implements IJoueurRest {

	WebTarget webTarget;
	WebTarget webTargetMultiPart;
	MicroserviceConfigProperties microserviceConfigProperties;
	@Resource(name = "dateFormatApi")
	DateFormat dateFormatApi;
	
	
	@Autowired
	public JoueurRestImpl(@Qualifier("clientJersey") Client client,
			@Qualifier("clientJerseyWithMultipart") Client clientMultipart,
			MicroserviceConfigProperties microserviceConfigProperties) {
		this.microserviceConfigProperties = microserviceConfigProperties;
		this.webTarget = client.target(microserviceConfigProperties.getBaseUrl());
		this.webTargetMultiPart = clientMultipart.target(microserviceConfigProperties.getBaseUrl());
	}
	
	@Override
	public List<Joueur> findAll() {
		WebTarget joueurWebTarget = webTarget.path(microserviceConfigProperties.getEndPointListeJoueurs());
		Invocation.Builder invocationBuilder = joueurWebTarget.request();
		return invocationBuilder.get(new GenericType<List<Joueur>>() {
		});
	}

	@Override
	public Joueur getOne(Integer id) {
		String endPointFindById = MessageFormat.format(microserviceConfigProperties.getEndPointJoueurFindById(),
				id);
		WebTarget joueurWebTarget = webTarget.path(endPointFindById);
		Invocation.Builder invocationBuilder = joueurWebTarget.request(MediaType.APPLICATION_JSON);
		Joueur joueur = invocationBuilder.get(Joueur.class);
		return joueur;
	}

	@Override
	public Joueur save(Joueur nouveauJoueur, MultipartFile fileImage) throws IOException {
		FormDataMultiPart formDataMultiPart = mapJoueurDtoToParamRequest(nouveauJoueur);

		if (fileImage != null && !fileImage.isEmpty()) {
			final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image",
					fileImage.getInputStream(), fileImage.getResource().getFilename());
			formDataMultiPart.bodyPart(streamDataBodyPart);
		}

		MultiPart multiPart = formDataMultiPart;
		WebTarget joueurWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointJoueurCreate());// Joueur
		// Construction requete http
		Invocation.Builder invocationBuilder = joueurWebTarget.request();
		return invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()), Joueur.class);
	}

	@Override
	public Joueur update(Joueur nouveauJoueur, MultipartFile fileImage) throws IOException {
		FormDataMultiPart formDataMultiPart = mapJoueurDtoToParamRequest(nouveauJoueur);

		if (fileImage != null && !fileImage.isEmpty()) {
			final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image",
					fileImage.getInputStream(), fileImage.getResource().getFilename());
			formDataMultiPart.bodyPart(streamDataBodyPart);
		}

		MultiPart multiPart = formDataMultiPart;
		WebTarget joueurWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointJoueurUpdate());// Joueur
		// Construction requete http
		Invocation.Builder invocationBuilder = joueurWebTarget.request();
		return invocationBuilder.put(Entity.entity(multiPart, multiPart.getMediaType()), Joueur.class);
	}

	@Override
	public void deleteById(Integer id) {
		String endPointCrud = MessageFormat.format(microserviceConfigProperties.getEndPointJoueurDelete(), id);
		WebTarget joueurWebTarget = webTarget.path(endPointCrud);
		Invocation.Builder invocationBuilder = joueurWebTarget.request();
		invocationBuilder.delete();
	}

//	@Override
//	public Joueur seLoger(String login, String password) {
//		String endPointSeLoger = microserviceConfigProperties.getEndPointJoueurSeLoger();
//		WebTarget joueurWebTarget = webTarget.path(endPointSeLoger).queryParam("login", login).queryParam("password",
//				password);
//		Invocation.Builder invocationBuilder = joueurWebTarget.request();
//		return invocationBuilder.get(Joueur.class);
//	}
	
	private FormDataMultiPart mapJoueurDtoToParamRequest(Joueur nouveauJoueur) {
		// utilisateur
		FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
		formDataMultiPart.field("id", nouveauJoueur.getId() != null ? nouveauJoueur.getId().toString() : "");
		formDataMultiPart.field("nom", nouveauJoueur.getNom() != null ? nouveauJoueur.getNom() : "");
		formDataMultiPart.field("prenom", nouveauJoueur.getPrenom() != null ? nouveauJoueur.getPrenom() : "");
		formDataMultiPart.field("email", nouveauJoueur.getEmail() != null ? nouveauJoueur.getEmail() : "");
		formDataMultiPart.field("password", nouveauJoueur.getPassword() != null ? nouveauJoueur.getPassword() : "");
		formDataMultiPart.field("dateNaissance",
				nouveauJoueur.getDateNaissance() != null ? dateFormatApi.format(nouveauJoueur.getDateNaissance())
						: "");
		formDataMultiPart.field("clubPrefere",
				nouveauJoueur.getClubPrefere() != null ? nouveauJoueur.getClubPrefere() : "");
		formDataMultiPart.field("clubActuel",
				nouveauJoueur.getClubActuel() != null ? nouveauJoueur.getClubActuel() : "");
		formDataMultiPart.field("telephone",
				nouveauJoueur.getTelephone() != null ? nouveauJoueur.getTelephone() : "");
		
		formDataMultiPart.field("tailleCm", nouveauJoueur.getTailleCm() != null ? nouveauJoueur.getTailleCm().toString() : "");
		formDataMultiPart.field("poidsKg", nouveauJoueur.getPoidsKg() != null ? nouveauJoueur.getPoidsKg().toString() : "");
		formDataMultiPart.field("piedFortDroit", nouveauJoueur.getPiedFortDroit() != null ? nouveauJoueur.getPiedFortDroit().toString() : "");
		formDataMultiPart.field("pathPhoto", nouveauJoueur.getPathPhoto() != null ? nouveauJoueur.getPathPhoto() : "");
		formDataMultiPart.field("description", nouveauJoueur.getDescription() != null ? nouveauJoueur.getDescription() : "");
		formDataMultiPart.field("vitesse100m", nouveauJoueur.getVitesse100m() != null ? nouveauJoueur.getVitesse100m().toString() : "");

		// Adresse
		if (nouveauJoueur.getAdresse() != null) {
			formDataMultiPart.field("id",
					nouveauJoueur.getAdresse().getId() != null ? nouveauJoueur.getAdresse().getId().toString() : "");
			formDataMultiPart.field("ligne1",
					nouveauJoueur.getAdresse().getLigne1() != null ? nouveauJoueur.getAdresse().getLigne1() : "");
			formDataMultiPart.field("ligne2",
					nouveauJoueur.getAdresse().getLigne2() != null ? nouveauJoueur.getAdresse().getLigne2() : "");
			formDataMultiPart.field("codePostal",
					nouveauJoueur.getAdresse().getCodePostal() != null ? nouveauJoueur.getAdresse().getCodePostal()
							: "");
			formDataMultiPart.field("ville",
					nouveauJoueur.getAdresse().getVille() != null ? nouveauJoueur.getAdresse().getVille() : "");
		}
		return formDataMultiPart;
	}
}
