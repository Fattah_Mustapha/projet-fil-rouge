package fr.afpa.cda.bared.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Competence;

public interface ICompetenceService {

	List<Competence> findAll();
	
	public Competence findById(Integer id);

	public Competence update(Competence nouvelleCompetence,MultipartFile file) throws IOException;

	public void deleteById(Integer id);

	public Competence save(Competence nouvelleCompetence, MultipartFile file) throws IOException;

}
