package fr.afpa.cda.bared.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Joueur;
import fr.afpa.cda.bared.webapp.bean.Personne;

public interface IJoueurService {
	public List<Joueur> findAll();

	public Personne findById(Integer id);

	public void deleteById(Integer id);

//	public Personne findByEmailAndPassword(String login, String password);

	public Joueur save(Joueur nouveauJoueur, MultipartFile file) throws IOException;

	Joueur update(Joueur nouveauJoueur, MultipartFile file) throws IOException;

}
