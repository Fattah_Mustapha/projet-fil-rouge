package fr.afpa.cda.bared.webapp.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Competence {
	private Integer id;
	private String nom;
	private String domaine;
	private List<Evaluation> evaluations = new ArrayList<>();
}
