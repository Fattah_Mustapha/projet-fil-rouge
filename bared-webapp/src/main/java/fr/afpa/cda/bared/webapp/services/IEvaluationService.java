package fr.afpa.cda.bared.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Evaluation;

public interface IEvaluationService {
	public List<Evaluation> findAll();

	public Evaluation findById(Integer id);

	public Evaluation update(Evaluation nouvelleEvaluation,MultipartFile file) throws IOException;

	public void deleteById(Integer id);

	public Evaluation findByJoueurIdAndCompetenceId(Integer idJoueur, Integer idCompetence);

	public Evaluation save(Evaluation nouvelleEvaluation, MultipartFile file) throws IOException;

}
