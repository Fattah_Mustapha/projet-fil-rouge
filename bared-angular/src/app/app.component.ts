import { Component } from '@angular/core';
import { Personne } from './shared/models/Personne';
import { PersonneService } from './shared/services/personne.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public connectedUser: Personne;

  constructor(private personneService: PersonneService) {
    // this.connectedUser = JSON.parse(localStorage.getItem('connectedUser'));
    this.personneService.connectedUser.subscribe((personne) => {
      this.connectedUser = personne;
    });
  }

  title = 'bared';
}
