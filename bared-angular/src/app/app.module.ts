import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { InscriptionPersonneComponent } from './inscription-personne/inscription-personne.component';
import { ErreurComponent } from './erreur/erreur.component';
import { InscriptionJoueurComponent } from './inscription-joueur/inscription-joueur.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { ProfilComponent } from './profil/profil.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { VoterComponent } from './voter/voter.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { PersonneService } from './shared/services/personne.service';
import { HttpClientModule } from '@angular/common/http';
import { Voter2Component } from './voter2/voter2.component';
import { ModifierProfilComponent } from './modifier-profil/modifier-profil.component';
import { VoterJoueurComponent } from './voter-joueur/voter-joueur.component';
import { EvaluationService } from './shared/services/evaluation.service';
import { VueJoueurComponent } from './vue-joueur/vue-joueur.component';
import { JoueurService } from './shared/services/joueur.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InscriptionPersonneComponent,
    ErreurComponent,
    InscriptionJoueurComponent,
    HomeComponent,
    ProfilComponent,
    ConnexionComponent,
    VoterComponent,
    Voter2Component,
    DeconnexionComponent,
    ModifierProfilComponent,
    VoterJoueurComponent,
    VueJoueurComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [PersonneService, EvaluationService, JoueurService],
  bootstrap: [AppComponent],
})
export class AppModule {}
