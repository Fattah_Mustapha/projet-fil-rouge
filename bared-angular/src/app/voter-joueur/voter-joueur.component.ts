import { Component, OnInit } from '@angular/core';
import { Joueur } from '../shared/models/Joueur';
import { Evaluation } from '../shared/models/Evaluation';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { EvaluationService } from '../shared/services/evaluation.service';

@Component({
  selector: 'app-voter-joueur',
  templateUrl: './voter-joueur.component.html',
  styleUrls: ['./voter-joueur.component.css'],
})
export class VoterJoueurComponent implements OnInit {
  public myForm: FormGroup;
  attributs: string[];
  joueur: Joueur;
  evaluation: Evaluation = new Evaluation();
  constructor(
    private evaluationService: EvaluationService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.joueur = JSON.parse(localStorage.getItem('evaluatedJoueur'));
    this.attributs = Object.getOwnPropertyNames(this.evaluation);

    // for (let attr of this.attributs) {
    //   for (let pos = 0; pos < attr.length; pos++) {
    //     if (attr.charCodeAt(pos) > 100 && attr.charCodeAt(pos) < 133) {
    //       // attr =
    //       //   attr.substring(0, pos) + ' ' + attr.substring(pos).toLowerCase();
    //       console.log(
    //         attr.substring(0, pos) +
    //           ' ' +
    //           attr.substring(pos, pos + 1).toLowerCase() +
    //           attr.substring(pos + 1)
    //       );
    //       attr = 'toto';
    //     }
    //   }
    // }

    console.log('Début', this.evaluation);
    this.myForm = this.fb.group({
      personneVotanteDto: '',
      evaluatedJoueurDto: '',
      dateEvaluation: '',
      centres: '',
      finition: '',
      jeuDeTete: '',
      passesCourtes: '',
      repriseDeVolee: '',
      dribble: '',
      effet: '',
      precisionCf: '',
      passesLongues: '',
      controleDuBallon: '',
      acceleration: '',
      vitesse: '',
      agilité: '',
      reactivite: '',
      equilibre: '',
      puissanceDeFrappe: '',
      detente: '',
      endurance: '',
      force: '',
      tirDeLoin: '',
      agressivite: '',
      interceptions: '',
      placement: '',
      visionDuJeu: '',
      penaltys: '',
      calme: '',
      marquage: '',
      tacle: '',
      tacleGlissé: '',
      jeuMain: '',
      jeuPied: '',
      placementGardien: '',
      reflexes: '',
    });
  }

  doSend() {
    if (this.myForm.status === 'VALID') {
      this.evaluation = this.myForm.value;
      this.evaluation.dateEvaluation = new Date();
      this.evaluation.evaluatedJoueurDto = JSON.parse(
        localStorage.getItem('evaluatedJoueur')
      );
      this.evaluation.personneVotanteDto = JSON.parse(
        localStorage.getItem('connectedUser')
      );
      console.log('Fin', this.evaluation);

      this.evaluationService
        .createEvaluation(this.evaluation)
        .subscribe((evaluation: Evaluation) => {
          if (evaluation) {
            console.log('Retour', evaluation);
            this.router.navigate(['/vue-joueur']);
          } else {
            alert('Erreur');
          }
        });
    }
  }

  doChange(event) {
    console.log(event);
    console.log(event.path);
    event.path[2].classList = 'card blue lighten-5';
  }
}
