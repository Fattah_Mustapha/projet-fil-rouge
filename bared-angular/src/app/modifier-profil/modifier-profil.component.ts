import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Personne } from '../shared/models/Personne';
import { Adresse } from '../shared/models/Adresse';
import { PersonneService } from '../shared/services/personne.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modifier-profil',
  templateUrl: './modifier-profil.component.html',
  styleUrls: ['./modifier-profil.component.css'],
})
export class ModifierProfilComponent implements OnInit {
  public myForm: FormGroup;
  public prenom = '';
  public personne: Personne;
  public adresse: Adresse;
  constructor(
    private personneService: PersonneService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.personne = JSON.parse(localStorage.getItem('connectedUser'));
    this.myForm = this.fb.group({
      id: this.personne.id,
      email: this.personne.email,
      password: this.personne.password,
      nom: this.personne.nom,
      prenom: this.personne.prenom,
      clubPrefere: this.personne.clubPrefere,
      clubActuel: this.personne.clubActuel,
      dateNaissance: this.personne.dateNaissance,
      telephone: this.personne.telephone,
      ligne1: this.personne.adresse.ligne1,
      ligne2: this.personne.adresse.ligne2,
      codePostal: this.personne.adresse.codePostal,
      ville: this.personne.adresse.ville,
    });
  }

  doSend() {
    if (this.myForm.status === 'VALID') {
      this.personne = this.myForm.value;
      this.adresse = JSON.parse(localStorage.getItem('connectedUser')).adresse;
      this.adresse.ligne2 = this.myForm.get('ligne1').value;
      this.adresse.ligne2 = this.myForm.get('ligne2').value;
      this.adresse.codePostal = this.myForm.get('codePostal').value;
      this.adresse.ville = this.myForm.get('ville').value;
      this.personne.adresse = this.adresse;
      console.log(this.personne);

      this.personneService
        .updatePersonne(this.personne)
        .subscribe((personne: Personne) => {
          if (personne) {
            localStorage.setItem('connectedUser', JSON.stringify(personne));
            this.personneService.connectedUser.next(personne);
            this.router.navigate(['/home']);
          } else {
            alert('Erreur');
          }
        });
    }
  }
}
