import { Component, OnInit } from '@angular/core';
import { Personne } from '../shared/models/Personne';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
})
export class ProfilComponent implements OnInit {
  personne: Personne;
  constructor() {}

  ngOnInit(): void {
    this.personne = JSON.parse(localStorage.getItem('connectedUser'));
  }
}
