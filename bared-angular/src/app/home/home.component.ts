import { Component, OnInit } from '@angular/core';
import { Personne } from '../shared/models/Personne';
import { PersonneService } from '../shared/services/personne.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public connectedUser: Personne;

  constructor(private personneService: PersonneService) {
    // this.connectedUser = JSON.parse(localStorage.getItem('connectedUser'));
    this.personneService.connectedUser.subscribe((personne) => {
      this.connectedUser = personne;
    });
  }

  ngOnInit() {
    console.log('connectedUser :');
    console.log(this.connectedUser);
  }
}
