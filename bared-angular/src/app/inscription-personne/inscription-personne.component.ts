import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { PersonneService } from '../shared/services/personne.service';
import { Personne } from '../shared/models/Personne';
import { Router } from '@angular/router';
import { Adresse } from '../shared/models/Adresse';

@Component({
  selector: 'app-inscription-personne',
  templateUrl: './inscription-personne.component.html',
  styleUrls: ['./inscription-personne.component.css'],
})
export class InscriptionPersonneComponent implements OnInit {
  public myForm: FormGroup;
  public prenom = '';
  public personne: Personne;
  public adresse: Adresse;
  constructor(
    private personneService: PersonneService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      id: '',
      email: '',
      password: '',
      nom: '',
      prenom: '',
      clubPrefere: '',
      clubActuel: '',
      dateNaissance: '',
      telephone: '',
      ligne1: '',
      ligne2: '',
      codePostal: '',
      ville: '',
    });
  }

  doSend() {
    if (this.myForm.status === 'VALID') {
      this.personne = this.myForm.value;
      this.adresse = new Adresse();
      this.adresse.ligne2 = this.myForm.get('ligne1').value;
      this.adresse.ligne2 = this.myForm.get('ligne2').value;
      this.adresse.codePostal = this.myForm.get('codePostal').value;
      this.adresse.ville = this.myForm.get('ville').value;
      this.personne.adresse = this.adresse;
      console.log(this.personne);

      this.personneService
        .createPersonne(this.personne)
        .subscribe((personne: Personne) => {
          if (personne) {
            localStorage.setItem('connectedUser', JSON.stringify(personne));
            this.personneService.connectedUser.next(personne);
            this.router.navigate(['/home']);
          } else {
            alert('Erreur');
          }
        });
    }
  }
}
