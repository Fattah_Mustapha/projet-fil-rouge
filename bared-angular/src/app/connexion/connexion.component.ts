import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PersonneService } from '../shared/services/personne.service';
import { Router } from '@angular/router';
import { Personne } from '../shared/models/Personne';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css'],
})
export class ConnexionComponent implements OnInit {
  public myForm: FormGroup;
  constructor(
    private personneService: PersonneService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      email: '',
      password: '',
    });
  }

  doSend() {
    if (this.myForm.status === 'VALID') {
      this.personneService
        .getUserByLoginPassword(
          this.myForm.get('email').value,
          this.myForm.get('password').value
        )
        .subscribe((personne: Personne) => {
          if (personne) {
            localStorage.setItem('connectedUser', JSON.stringify(personne));
            this.personneService.connectedUser.next(personne);
            this.router.navigate(['/home']);
          } else {
            alert('Erreur');
          }
        });
    }
  }
}
