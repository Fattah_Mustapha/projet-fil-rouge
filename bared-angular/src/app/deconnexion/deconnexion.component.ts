import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../shared/services/personne.service';

@Component({
  selector: 'app-deconnexion',
  templateUrl: './deconnexion.component.html',
  styleUrls: ['./deconnexion.component.css'],
})
export class DeconnexionComponent implements OnInit {
  constructor(private personneService: PersonneService) {}

  ngOnInit(): void {
    localStorage.removeItem('connectedUser');
    this.personneService.connectedUser.next(null);
  }
}
