import { Injectable } from '@angular/core';
import { Joueur } from '../models/Joueur';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class JoueurService {
  public connectedUser: BehaviorSubject<Joueur> = new BehaviorSubject<Joueur>(
    JSON.parse(localStorage.getItem('connectedUser'))
  );

  constructor(private http: HttpClient) {}

  public createJoueur(joueur: Joueur): Observable<Joueur> {
    return this.http.post<Joueur>('http://localhost:9091//joueurs', joueur);
  }

  public updateJoueur(joueur: Joueur): Observable<Joueur> {
    return this.http.put<Joueur>('http://localhost:9091//joueurs', joueur);
  }

  public getUserByLoginPassword(
    email: string,
    password: string
  ): Observable<Joueur> {
    return this.http.get<Joueur>(
      'http://localhost:9091/joueurs/login' +
        '?login=' +
        email +
        '&password=' +
        password
    );
  }

  public getJoueurByNomOrPrenom(
    nom: string,
    prenom: string
  ): Observable<Joueur[]> {
    return this.http.get<Joueur[]>(
      'http://localhost:9091/joueurs/rechercheParNomPrenom' +
        '?nom=' +
        nom +
        '&prenom=' +
        prenom
    );
  }

  public getJoueurById(id: number): Observable<Joueur> {
    return this.http.get<Joueur>('http://localhost:9091/joueurs/' + id);
  }
}
