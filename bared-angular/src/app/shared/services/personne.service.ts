import { Injectable } from '@angular/core';
import { Personne } from '../models/Personne';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class PersonneService {
  public connectedUser: BehaviorSubject<Personne> = new BehaviorSubject<
    Personne
  >(JSON.parse(localStorage.getItem('connectedUser')));

  constructor(private http: HttpClient) {}

  public getPersonnesById(id: string): Observable<Personne> {
    return this.http.get<Personne>('http://localhost:9091/personnes/' + id);
  }
  public createPersonne(personne: Personne): Observable<Personne> {
    return this.http.post<Personne>(
      'http://localhost:9091//personnes',
      personne
    );
  }

  public updatePersonne(personne: Personne): Observable<Personne> {
    return this.http.put<Personne>(
      'http://localhost:9091//personnes',
      personne
    );
  }

  public getUserByLoginPassword(
    email: string,
    password: string
  ): Observable<Personne> {
    return this.http.get<Personne>(
      'http://localhost:9091/personnes/login' +
        '?login=' +
        email +
        '&password=' +
        password
    );
  }
  // public getJoueurByNomOrPrenom(
  //   nom: string,
  //   prenom: string
  // ): Observable<Personne[]> {
  //   return this.http.get<Personne[]>(
  //     'http://localhost:9091/personnes/rechercheParNomPrenom' +
  //       '?nom=' +
  //       nom +
  //       '&prenom=' +
  //       prenom
  //   );
  // }

  //   public existLogin(login: string): boolean {
  //   return null;
  // }
  //   public getAll(): Observable<Personne[] > {
  //     return this.http.get<Array<Personne>>("http://localhost:9091/utilisateurs");
  // }
}
