import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Evaluation } from '../models/Evaluation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EvaluationService {
  constructor(private http: HttpClient) {}
  public createEvaluation(evaluation: Evaluation): Observable<Evaluation> {
    return this.http.post<Evaluation>(
      'http://localhost:9091//evaluations',
      evaluation
    );
  }

  public getEvaluationsByIdJoueur(id: number): Observable<Evaluation[]> {
    return this.http.get<Evaluation[]>(
      'http://localhost:9091//evaluations/joueur/' + id
    );
  }
}
