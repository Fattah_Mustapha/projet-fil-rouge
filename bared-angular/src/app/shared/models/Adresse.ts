export class Adresse {
  public id: number;
  public ligne1: string;
  public ligne2: string;
  public codePostal: string;
  public ville: string;
  constructor() {}
}
