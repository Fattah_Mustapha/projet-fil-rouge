import { Personne } from './Personne';
import { Evaluation } from './Evaluation';

export class Joueur extends Personne {
  public tailleCm: number;
  public poidsKg: number;
  public piedFortDroit: boolean;
  public pathPhoto: string;
  public description: string;
  public vitesse100m: number;
  public evaluations: Evaluation[];
  public url: string;
  public moyennes: number[];
}
