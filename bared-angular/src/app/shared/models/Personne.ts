import { Adresse } from './Adresse';

export class Personne {
  constructor(
    public id: number,
    public nom: string,
    public prenom: string,
    public email: string,
    public password: string,
    public clubPrefere: string,
    public clubActuel: string,
    public dateNaissance: Date,
    public telephone: string,
    public adresse: Adresse
  ) {}
}
