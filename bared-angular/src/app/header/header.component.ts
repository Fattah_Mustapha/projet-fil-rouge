import { Component, OnInit } from '@angular/core';
import { Personne } from '../shared/models/Personne';
import { PersonneService } from '../shared/services/personne.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  public connectedUser: Personne;

  constructor(private personneService: PersonneService) {
    this.personneService.connectedUser.subscribe((personne) => {
      this.connectedUser = personne;
    });
  }

  ngOnInit(): void {
    // this.connectedUser = JSON.parse(localStorage.getItem('connectedUser'));
    console.log('connectedUser :');
    console.log(this.connectedUser);
    // this.personneService.connectedUser.subscribe((value) => {
    //   this.connectedUser = value;
    // });
  }
}
