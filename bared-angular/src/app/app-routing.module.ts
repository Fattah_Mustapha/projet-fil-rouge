import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InscriptionPersonneComponent } from './inscription-personne/inscription-personne.component';
import { ErreurComponent } from './erreur/erreur.component';
import { InscriptionJoueurComponent } from './inscription-joueur/inscription-joueur.component';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { VoterComponent } from './voter/voter.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { AppComponent } from './app.component';
import { Voter2Component } from './voter2/voter2.component';
import { ModifierProfilComponent } from './modifier-profil/modifier-profil.component';
import { VoterJoueurComponent } from './voter-joueur/voter-joueur.component';
import { VueJoueurComponent } from './vue-joueur/vue-joueur.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'inscription-personne', component: InscriptionPersonneComponent },
  { path: 'inscription-joueur', component: InscriptionJoueurComponent },
  { path: 'mon-profil', component: ProfilComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'voter', component: VoterComponent },
  { path: 'voter-liste', component: Voter2Component },
  { path: 'voter-joueur', component: VoterJoueurComponent },
  { path: 'vue-joueur', component: VueJoueurComponent },
  { path: 'deconnexion', component: DeconnexionComponent },
  { path: 'modifier-profil', component: ModifierProfilComponent },
  // { path: 'details-produit/:id', component: DetailsProduitComponent }
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '**', component: ErreurComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
