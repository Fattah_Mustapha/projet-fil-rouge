import { Component, OnInit } from '@angular/core';
import { Joueur } from '../shared/models/Joueur';
import { Router } from '@angular/router';
import { JoueurService } from '../shared/services/joueur.service';

@Component({
  selector: 'app-voter2',
  templateUrl: './voter2.component.html',
  styleUrls: ['./voter2.component.css'],
})
export class Voter2Component implements OnInit {
  joueurs: Joueur[];
  constructor(private router: Router, private joueurService: JoueurService) {}

  ngOnInit(): void {
    this.joueurs = JSON.parse(localStorage.getItem('joueurs'));
    if (this.joueurs.length === 0) {
      this.joueurs = null;
    }
  }

  voterJoueur(joueur: Joueur) {
    console.log(joueur);
    localStorage.setItem('evaluatedJoueur', JSON.stringify(joueur));
    this.router.navigate(['/voter-joueur']);
  }
}
