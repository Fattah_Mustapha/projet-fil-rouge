package fr.afpa.cda.bared.microservice.dao.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Joueur extends Personne {
	private Integer tailleCm;
	private Integer poidsKg;
	private Boolean piedFortDroit;
	private String pathPhoto;
	private String description;
	private Float vitesse100m;
	@OneToMany
	private List<Evaluation> evaluations;
	
}
