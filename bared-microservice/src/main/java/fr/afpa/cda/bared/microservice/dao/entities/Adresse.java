package fr.afpa.cda.bared.microservice.dao.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Data
@Entity
@SequenceGenerator(name = "adresse_id_seq",initialValue = 1,allocationSize = 1)
public class Adresse {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adresse_id_seq")
	private Integer id;
	private String ligne1;
	private String ligne2;
	private String codePostal;
	private String ville;
	
}
