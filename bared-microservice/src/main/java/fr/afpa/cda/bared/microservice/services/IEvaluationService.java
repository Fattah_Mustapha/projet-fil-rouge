package fr.afpa.cda.bared.microservice.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.EvaluationDto;

public interface IEvaluationService {
	public List<EvaluationDto> findAll();

	public EvaluationDto findEvaluationById(Integer id);

	public void deleteById(Integer id);

	public List<EvaluationDto> findByJoueurId(Integer IdJoueur);
	
	public EvaluationDto update(EvaluationDto evaluationDto);
	
	public EvaluationDto save(EvaluationDto evaluationDto);

}
