package fr.afpa.cda.bared.microservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.bared.microservice.dao.entities.Evaluation;

@Repository
public interface IEvaluationDao extends JpaRepository<Evaluation, Integer> {

	// recherche par nom de methode
	List<Evaluation> findByEvaluatedJoueur_id(Integer joueurId);

}
