package fr.afpa.cda.bared.microservice.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.CompetenceDto;

public interface ICompetenceService {
	public List<CompetenceDto> findAllCompetences();

	public CompetenceDto findCompetenceById(Integer id);

	public void deleteById(Integer id);

	public CompetenceDto findByNom(String nom);

	public List<CompetenceDto> findByDomaine(String domaine);

	public CompetenceDto update(CompetenceDto competenceDto);

	public CompetenceDto update(CompetenceDto competenceDto, MultipartFile uneImage);

	public CompetenceDto save(CompetenceDto competenceDto);

	public CompetenceDto save(CompetenceDto competenceDto, MultipartFile uneImage);
}
