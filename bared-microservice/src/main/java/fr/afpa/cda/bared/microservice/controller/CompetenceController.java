package fr.afpa.cda.bared.microservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.CompetenceDto;
import fr.afpa.cda.bared.microservice.services.ICompetenceService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;

@RestController
public class CompetenceController {

	@Autowired
	ICompetenceService competenceService;

	@Autowired
	IFileSystemStorageService fileSystemStorageService;

	@GetMapping({ "/competences" })
	public List<CompetenceDto> getAll() {
		List<CompetenceDto> competences = competenceService.findAllCompetences();
		return competences;
	}

	// DELETE /competences/{id}
	@DeleteMapping("/competences/{id}")
	public void supprimerCompetence(@PathVariable Integer id) {
		competenceService.deleteById(id);
		System.out.println("Competence Supprimée");
	}

	// GET /competences/{id} ==> getById
	@GetMapping("/competences/{id}")
	public CompetenceDto chercherCompetenceParId(@PathVariable Integer id, HttpServletRequest request) {
		return competenceService.findCompetenceById(id);
	}

	@PostMapping("/competences")
	@ResponseStatus(value = HttpStatus.CREATED)
	public CompetenceDto save(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			CompetenceDto competenceDto) {
		System.out.println(competenceDto);
		if (uneImage != null && !uneImage.isEmpty())
			return competenceService.save(competenceDto, uneImage);

		return competenceService.save(competenceDto);
	}

	@PutMapping("/competences")
	@ResponseStatus(value = HttpStatus.OK)
	public CompetenceDto update(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			CompetenceDto competenceDto) {
		if (uneImage != null && !uneImage.isEmpty())
			return competenceService.update(competenceDto, uneImage);
		return competenceService.update(competenceDto);
	}

}
