package fr.afpa.cda.bared.microservice.dao.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import lombok.Data;

@Data
@Entity
@SequenceGenerator(name = "evaluation_id_seq", initialValue = 1, allocationSize = 1)
public class Evaluation {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evaluation_id_seq")
	private Integer id;
	@ManyToOne
	private Personne personneVotante;
	@ManyToOne
	private Joueur evaluatedJoueur;
	@Temporal(TemporalType.DATE)
	private Date dateEvaluation;
	private Integer centres;
	private Integer finition;
	private Integer jeuDeTete;
	private Integer passesCourtes;
	private Integer repriseDeVolee;
	private Integer dribble;
	private Integer effet;
	private Integer precisionCf;	
	private Integer passesLongues;
	private Integer controleDuBallon;
	private Integer acceleration;
	private Integer vitesse;
	private Integer agilité;
	private Integer reactivite;
	private Integer equilibre;
	private Integer puissanceDeFrappe;
	private Integer detente;
	private Integer endurance;
	private Integer force;
	private Integer tirDeLoin;
	private Integer agressivite;
	private Integer interceptions;
	private Integer placement;
	private Integer visionDuJeu;
	private Integer penaltys;
	private Integer calme;
	private Integer marquage;
	private Integer tacle;
	private Integer tacleGlissé;
	private Integer jeuMain;
	private Integer jeuPied;
	private Integer placementGardien;
	private Integer reflexes;
}
