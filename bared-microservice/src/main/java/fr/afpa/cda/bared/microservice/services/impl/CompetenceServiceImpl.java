package fr.afpa.cda.bared.microservice.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.CompetenceDto;
import fr.afpa.cda.bared.microservice.dao.ICompetenceDao;
import fr.afpa.cda.bared.microservice.dao.entities.Competence;
import fr.afpa.cda.bared.microservice.properties.StorageProperties;
import fr.afpa.cda.bared.microservice.services.ICompetenceService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;

@Service
public class CompetenceServiceImpl implements ICompetenceService {

	@Autowired
	ICompetenceDao competenceDao;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	IFileSystemStorageService fileSystemStorageService;
	@Autowired
	StorageProperties storageProperties;

	@Override
	public List<CompetenceDto> findAllCompetences() {

		return competenceDao.findAll().stream().map(entity -> modelMapper.map(entity, CompetenceDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public CompetenceDto findCompetenceById(Integer id) {
		Competence competence = competenceDao.getOne(id);
		if (competence == null) {
			return null;
		}
		return modelMapper.map(competence, CompetenceDto.class);
	}

	@Override
	public void deleteById(Integer id) {
		competenceDao.deleteById(id);

	}

	@Override
	public CompetenceDto save(CompetenceDto nouveauCompetence) {
		Competence paramToDo = modelMapper.map(nouveauCompetence, Competence.class);
		paramToDo = competenceDao.save(paramToDo);
		return modelMapper.map(paramToDo, CompetenceDto.class);
	}

	@Override
	public CompetenceDto save(CompetenceDto competenceDto, MultipartFile uneImage) {
		Competence competenceResult = competenceDao.save(modelMapper.map(competenceDto, Competence.class));
		return modelMapper.map(competenceResult, CompetenceDto.class);
	}

	@Override
	public CompetenceDto update(CompetenceDto competenceModifie) {
		Competence paramToDo = modelMapper.map(competenceModifie, Competence.class);
		paramToDo = competenceDao.save(paramToDo);
		return modelMapper.map(paramToDo, CompetenceDto.class);
	}

	@Override
	public CompetenceDto update(CompetenceDto personneModifiee, MultipartFile uneImage) {
		Competence competenceResult = modelMapper.map(personneModifiee, Competence.class);
		return modelMapper.map(competenceResult, CompetenceDto.class);
	}

	@Override
	public CompetenceDto findByNom(String nom) {
		Competence competence = competenceDao.findByNom(nom);
		if (competence == null) {
			return null;
		}
		return modelMapper.map(competence, CompetenceDto.class);
	}

	@Override
	public List<CompetenceDto> findByDomaine(String domaine) {
		return competenceDao.findByDomaine(domaine).stream().map(entity -> modelMapper.map(entity, CompetenceDto.class))
				.collect(Collectors.toList());
	}
}
