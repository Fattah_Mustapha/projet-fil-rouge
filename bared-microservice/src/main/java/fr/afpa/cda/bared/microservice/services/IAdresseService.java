package fr.afpa.cda.bared.microservice.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.AdresseDto;

public interface IAdresseService {

	List<AdresseDto> findAll();

	AdresseDto findById(Integer id);

	void deleteById(Integer id);

	AdresseDto save(AdresseDto nouvelleAdresse);

	AdresseDto update(AdresseDto adresseModifiee);

	AdresseDto findByAll(AdresseDto adresseDto);

}
