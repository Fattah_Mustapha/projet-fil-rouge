package fr.afpa.cda.bared.microservice.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.PersonneDto;

public interface IPersonneService {
	public List<PersonneDto> findAll();

	public PersonneDto findPersonneById(Integer id);

	public void deleteById(Integer id);

	public PersonneDto findByEmailAndPassword(String login, String password);
	
//	List<PersonneDto> findByNomOrPrenom(String nomOrPrenom);
	
	/**
	 * metre à jour un utilisateur sans sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public PersonneDto update(PersonneDto personneDto);

	/**
	 * metre à jour un utilisateur avec sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public PersonneDto update(PersonneDto personneDto, MultipartFile uneImage);

	/**
	 * creation un utilisateur sans sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public PersonneDto save(PersonneDto personneDto);

	/**
	 * creation un utilisateur avec sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public PersonneDto save(PersonneDto personneDto, MultipartFile uneImage);

	public List<PersonneDto> getPersonneByNomOrPrenom(String nom, String prenom);
}
