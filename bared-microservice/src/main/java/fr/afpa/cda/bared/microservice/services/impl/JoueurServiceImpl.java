package fr.afpa.cda.bared.microservice.services.impl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.JoueurDto;
import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import fr.afpa.cda.bared.microservice.dao.IJoueurDao;
import fr.afpa.cda.bared.microservice.dao.entities.Joueur;
import fr.afpa.cda.bared.microservice.dao.entities.Personne;
import fr.afpa.cda.bared.microservice.properties.StorageProperties;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;
import fr.afpa.cda.bared.microservice.services.IJoueurService;

@Service
public class JoueurServiceImpl implements IJoueurService {

	@Autowired
	IJoueurDao joueurDao;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	IFileSystemStorageService fileSystemStorageService;
	@Autowired
	StorageProperties storageProperties;

	@Override
	public List<JoueurDto> findAll() {

		return joueurDao.findAll().stream().map(entity -> modelMapper.map(entity, JoueurDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public JoueurDto findJoueurById(Integer id) {
		Joueur joueur = joueurDao.getOne(id);
		if (joueur == null) {
			return null;
		}
		return modelMapper.map(joueur, JoueurDto.class);
	}

	@Override
	public void deleteById(Integer id) {
		joueurDao.deleteById(id);

	}

	@Override
	public JoueurDto findByEmailAndPassword(String login, String password) {
		Joueur joueur = joueurDao.findByEmailAndPassword(login, password);
		if (joueur == null) {
			return null;
		}
		return modelMapper.map(joueur, JoueurDto.class);

	}

	@Override
	public JoueurDto save(JoueurDto nouveauJoueur) {

		Joueur paramToDo = modelMapper.map(nouveauJoueur, Joueur.class);
		paramToDo = joueurDao.save(paramToDo);
		return modelMapper.map(paramToDo, JoueurDto.class);
	}

	@Override
	public JoueurDto save(JoueurDto joueurDto, MultipartFile uneImage) {
		Joueur joueurResult = joueurDao.save(modelMapper.map(joueurDto, Joueur.class));
		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), joueurResult.getId());
		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
		joueurResult.setPathPhoto(path.toString());
		joueurResult = joueurDao.save(joueurResult);
		return modelMapper.map(joueurResult, JoueurDto.class);
	}

	@Override
	public JoueurDto update(JoueurDto joueurModifie) {
		// chercher l'ancien utilisateur en BDD
		Joueur oldJoueur = joueurDao.findById(joueurModifie.getId()).get();
		// remettre le chemin de l'image (non updaté dans cette méthode) dans le nouvel
		// utilisateur
		Joueur paramToDo = modelMapper.map(joueurModifie, Joueur.class);
		paramToDo.setPathPhoto(oldJoueur.getPathPhoto());
		// sauvegarder le nouvel utilisateur
		paramToDo = joueurDao.save(paramToDo);
		return modelMapper.map(paramToDo, JoueurDto.class);
	}

	@Override
	public JoueurDto update(JoueurDto personneModifiee, MultipartFile uneImage) {
		Joueur joueurResult = modelMapper.map(personneModifiee, Joueur.class);
		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), joueurResult.getId());
		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
		joueurResult.setPathPhoto(path.toString());
		joueurResult = joueurDao.save(joueurResult);
		return modelMapper.map(joueurResult, JoueurDto.class);
	}

	@Override
	public List<JoueurDto> findByNomOrPrenom(String nomOrPrenom) {
		return joueurDao.findByNomOrPrenom(nomOrPrenom, nomOrPrenom).stream()
				.map(entity -> modelMapper.map(entity, JoueurDto.class)).collect(Collectors.toList());
	}
	
	@Override
	public List<JoueurDto> getJoueurByNomOrPrenom(String nom, String prenom) {
		List<Joueur> joueurs = joueurDao.findJoueurByNomContainingIgnoreCaseOrPrenomContainingIgnoreCase(nom, prenom);
		System.out.println(joueurs);
		return joueurs.stream().map(entity -> modelMapper.map(entity, JoueurDto.class))
				.collect(Collectors.toList());
		
	}
}
