package fr.afpa.cda.bared.microservice.bean;

import lombok.Data;

@Data
public class AdresseDto {
	private Integer id;
	private String ligne1;
	private String ligne2;
	private String codePostal;
	private String ville;
}
