package fr.afpa.cda.bared.microservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.AdresseDto;
import fr.afpa.cda.bared.microservice.bean.JoueurDto;
import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import fr.afpa.cda.bared.microservice.services.IAdresseService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;
import fr.afpa.cda.bared.microservice.services.IJoueurService;

@RestController
@CrossOrigin(origins = "*")
public class JoueurController {

	@Autowired
	IJoueurService joueurService;

	@Autowired
	IAdresseService adresseService;

	@Autowired
	IFileSystemStorageService fileSystemStorageService;

	@GetMapping({ "/joueurs" })
	public List<JoueurDto> getAll() {
		List<JoueurDto> joueurs = joueurService.findAll();
		return joueurs;
	}

	// DELETE /users/{id}
	@DeleteMapping("/joueurs/{id}")
	public void supprimerJoueur(@PathVariable Integer id) {
		joueurService.deleteById(id);
		System.out.println("Joueur Supprimé");
	}

	// GET /joueurs/{id} ==> getById
	@GetMapping("/joueurs/{id}")
	public JoueurDto chercherJoueurParId(@PathVariable Integer id, HttpServletRequest request) {
		System.out.println("Joueur : " +joueurService.findJoueurById(id));
		return joueurService.findJoueurById(id);
	}

	@PostMapping("/joueurs")
	@ResponseStatus(value = HttpStatus.CREATED)
	public JoueurDto save(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			JoueurDto joueurDto, AdresseDto adresseDto) {
		adresseDto=adresseService.findByAll(adresseDto);
		if(adresseDto.getId()==null) {
			adresseDto = adresseService.save(adresseDto);
		}
		joueurDto.setAdresse(adresseDto);
		System.out.println(joueurDto);
		if (uneImage != null && !uneImage.isEmpty())
			return joueurService.save(joueurDto, uneImage);

		return joueurService.save(joueurDto);
	}

	@PutMapping("/joueurs")
	@ResponseStatus(value = HttpStatus.OK)
	public JoueurDto update(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			JoueurDto joueurDto, AdresseDto adresseDto) {
		adresseDto=adresseService.update(adresseDto);
		joueurDto.setAdresse(adresseDto);
		if (uneImage != null && !uneImage.isEmpty())
			return joueurService.update(joueurDto, uneImage);
		return joueurService.update(joueurDto);
	}

	@GetMapping("/joueurs/login")
	public JoueurDto seLoger(String login, String password) {
		return joueurService.findByEmailAndPassword(login, password);
	}
	
	@GetMapping({ "/joueurs/rechercheParNomPrenom" })
	public List<JoueurDto> getJoueurByNomOrPrenom(String nom, String prenom) {
		List<JoueurDto> utilisateurs = joueurService.getJoueurByNomOrPrenom(nom, prenom);
		return utilisateurs;
	}

}
