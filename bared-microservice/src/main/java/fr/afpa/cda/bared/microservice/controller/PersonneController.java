package fr.afpa.cda.bared.microservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.AdresseDto;
import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import fr.afpa.cda.bared.microservice.services.IAdresseService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;
import fr.afpa.cda.bared.microservice.services.IPersonneService;

@RestController
@CrossOrigin(origins = "*")
public class PersonneController {

	@Autowired
	IPersonneService personneService;

	@Autowired
	IAdresseService adresseService;

	@Autowired
	IFileSystemStorageService fileSystemStorageService;

	@GetMapping({ "/personnes" })
	public List<PersonneDto> getAll() {
		List<PersonneDto> utilisateurs = personneService.findAll();
		return utilisateurs;
	}

	// DELETE /users/{id}
	@DeleteMapping("/personnes/{id}")
	public void supprimerPersonne(@PathVariable Integer id) {
		personneService.deleteById(id);
		System.out.println("Personne Supprimée");
	}

	// GET /personness/{id} ==> getById
	@GetMapping("/personnes/{id}")
	public PersonneDto chercherPersonneParId(@PathVariable Integer id, HttpServletRequest request) {
		return personneService.findPersonneById(id);
	}

//	@PostMapping("/personnes")
//	@ResponseStatus(value = HttpStatus.CREATED)
//	public PersonneDto save(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
//			PersonneDto personneDto, AdresseDto adresseDto) {
//		adresseDto=adresseService.findByAll(adresseDto);
//		System.out.println("adre "+adresseDto);
//		if(adresseDto.getId()==null) {
//			adresseDto = adresseService.save(adresseDto);
//		}
//		personneDto.setAdresse(adresseDto);
//		System.out.println(personneDto);
//		// System.out.println(adresseDto);
////		if (uneImage != null && !uneImage.isEmpty())
////			return personneService.save(personneDto, uneImage);
//
//		return personneService.save(personneDto);
//	}
	
	
	
	@PostMapping("/personnes")
	@ResponseStatus(value = HttpStatus.CREATED)
	public PersonneDto save(@RequestBody PersonneDto personneDto) {
		System.out.println(personneDto);
		return personneService.save(personneDto);
	}

//	@PutMapping("/personnes")
//	@ResponseStatus(value = HttpStatus.OK)
//	public PersonneDto update(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
//			PersonneDto personneDto, AdresseDto adresseDto) {
////		adresseDto= adresseService.findById(adresseDto.getId());
//		adresseDto=adresseService.update(adresseDto);
//		personneDto.setAdresse(adresseDto);
//		if (uneImage != null && !uneImage.isEmpty())
//			return personneService.update(personneDto, uneImage);
//		return personneService.update(personneDto);
//	}
	
	@PutMapping("/personnes")
	@ResponseStatus(value = HttpStatus.OK)
	public PersonneDto update(@RequestBody PersonneDto personneDto) {
//		adresseDto=adresseService.update(adresseDto);
//		personneDto.setAdresse(adresseDto);
		System.out.println(personneDto);
		return personneService.update(personneDto);
	}


	@GetMapping("/personnes/login")
	public PersonneDto seLoger(String login, String password) {
		return personneService.findByEmailAndPassword(login, password);
	}
	
	@GetMapping({ "/personnes/rechercheParNomPrenom" })
	public List<PersonneDto> getPersonneByNomOrPrenom(String nom, String prenom) {
		List<PersonneDto> utilisateurs = personneService.getPersonneByNomOrPrenom(nom, prenom);
		return utilisateurs;
	}

}
