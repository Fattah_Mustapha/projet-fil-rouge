package fr.afpa.cda.bared.microservice.bean;

import java.nio.file.Paths;
import java.util.List;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import fr.afpa.cda.bared.microservice.controller.FileUploadController;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class JoueurDto extends PersonneDto {
	private Integer tailleCm;
	private Integer poidsKg;
	private Boolean piedFortDroit;
	private String pathPhoto;
	private String description;
	private Float vitesse100m;
	private List<EvaluationDto> evaluations;
	private String url;

	public String getUrl() {
		if (pathPhoto == null || this.getId() == null)
			return null;
		this.url = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveProfil", this.getId(),
				Paths.get(this.pathPhoto).getFileName().toString()).build().toString();

		return this.url;
	}
}
