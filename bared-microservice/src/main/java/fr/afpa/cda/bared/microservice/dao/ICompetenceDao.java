package fr.afpa.cda.bared.microservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.bared.microservice.dao.entities.Competence;

@Repository
public interface ICompetenceDao extends JpaRepository<Competence, Integer> {

	// recherche par nom de methode
	List<Competence> findByDomaine(String domaine);

	Competence findByNom(String nom);

}
