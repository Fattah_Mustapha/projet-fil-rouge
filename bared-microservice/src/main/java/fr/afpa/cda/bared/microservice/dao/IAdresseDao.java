package fr.afpa.cda.bared.microservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.bared.microservice.dao.entities.Adresse;

@Repository
public interface IAdresseDao extends JpaRepository<Adresse, Integer>{
	Adresse findAdresseByLigne1AndLigne2AndCodePostalAndVille (String ligne1, String ligne2, String codePostal, String ville);

}
