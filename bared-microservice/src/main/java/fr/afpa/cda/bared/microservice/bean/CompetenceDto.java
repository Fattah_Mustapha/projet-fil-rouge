package fr.afpa.cda.bared.microservice.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CompetenceDto {
	private Integer id;
	private String nom;
	private String domaine;
	private List<EvaluationDto> evaluations = new ArrayList<>();
}
