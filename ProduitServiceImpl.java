package fr.afpa.cda.ecommerce.microservice.app.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.cda.ecommerce.microservice.app.bean.ProduitDto;
import fr.afpa.cda.ecommerce.microservice.app.dao.IProduitDao;
import fr.afpa.cda.ecommerce.microservice.app.dao.entities.Produit;
import fr.afpa.cda.ecommerce.microservice.app.services.IProduitService;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ProduitServiceImpl implements IProduitService {

	@Autowired
	IProduitDao produitDao;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public ProduitDto save(ProduitDto newProduit) {

		Produit newProduitDO = modelMapper.map(newProduit, Produit.class);
		newProduitDO = produitDao.save(newProduitDO);
		ProduitDto savedProduitDto = modelMapper.map(newProduitDO, ProduitDto.class);
		return savedProduitDto;
	}

	@Override
	public ProduitDto update(ProduitDto newProduitDto) {
		Produit newProduitDO = modelMapper.map(newProduitDto, Produit.class);
		newProduitDO = produitDao.save(newProduitDO);
		ProduitDto savedProduitDto = modelMapper.map(newProduitDO, ProduitDto.class);
		return savedProduitDto;
	}

	@Override
	public ProduitDto findById(Integer id) {
		Produit resultatDO = produitDao.findById(id).get();
		return modelMapper.map(resultatDO, ProduitDto.class);
	}

	@Override
	public List<ProduitDto> findAll() {
		return produitDao.findAll().stream().map(entity -> modelMapper.map(entity, ProduitDto.class))
				.collect(Collectors.toList());

	}

	@Override
	public void deleteById(Integer id) {
		produitDao.deleteById(id);
	}
}
